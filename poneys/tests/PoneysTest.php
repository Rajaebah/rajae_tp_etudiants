<?php
 // require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {


	protected $poneys ; 

	public function setUp(){
			$this->poneys = new Poneys; 
			$this->poneys->setCount(10);
		}
	

	 /**
    	 * @dataProvider dataTest
     	*/

    	public function test_removePoneyFromField() {
      	// Setup
      	//$Poneys = new Poneys();

      	// Action
     	$this->poneys->removePoneyFromField(3);
      
      	//Assert
      	$this->assertEquals(5, $this->poneys->getCount());


      	//Add poneys 
     	$this->poneys->addPoneyToField(12); 

	//Assert Add 
	$this->assertEquals(6,$this->poneys->getCount());

	//Free places 
	$this->assertEquals(true,$this->poneys->isPlaceFree());

	}

	public function dataTest(){

		return [ [2] , [3] , [0], [-4], [10]]; 
	}

	public function textNames(){ 

	$this->poneys = $this->getMockBuilder('Poneys')->getMock(); 
	$this->poneys
	 	->expects($this->exactly())
		->method('getNames')
		->willReturn(array('A','B','C','D')); 
	$this->assertEquals(array('A','B','C','D'),$this->poneys->getNames());

	}

	public function tearDown() {
		$this->poneys = null; 
	}
	
  }
 ?>
