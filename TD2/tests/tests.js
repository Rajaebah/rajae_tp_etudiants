QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});

QUnit.test( "pair()" , function( assert ) {
  assert.expect (4)

  var res = pair(1); 
  assert.equal(res , false , " 1 est impair"); 
  var res = pair(3); 
  assert.equal(res , false , " 3 est impair"); 
  var res = pair(6); 
  assert.equal(res , true , " 6 est pair"); 
  var res = pair(4); 
  assert.equal(res , true , " 4 est pair"); 
});


QUnit.test("bonjour()" , function(assert){ 
  var res = bonjour("rajae") ;
  assert.ok ( res == "bonjour rajae" , "Passed ! "); 

}); 
